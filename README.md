# PF PWA
Magento2.3 pwa with pfconcept magento data source.

### Prerequisites
- docker (docker command should be accessable by user)
  - `sudo usermod -aG docker ${USER}`
  - reboot machine
- docker-compose

### Steps to setup dev server
- Clone the repo to local
- `docker-compose build`  if this is your first time
- `docker-compose up`
  - From next time
    - `docker-compose start` to start server
    - `docker-compose stop` to stop server
  - Don't use `docker-compose down` (it will wipe current instance)
- `docker-compose exec web bash` to login into container
- `cd /pwa/` to goto project's root dir
- `yarn install` if this is your first time
  - PS: there might be errors printed, plz ignore them.
- `cp packages/venia-concept/.env.dev packages/venia-concept/.env`
  - We are using `.env.dev` file which is modified of default `.dev.dist`
- `yarn run build` if this is your first time
- `yarn run watch:venia` to start venia serve server

### Suggestions
- IDE: WebStorm | Editor: VS Code with all necessary plugins
- Open `<project_folder>/pwa/packages/venia-concept/` dir as project root in IDE or Editor for better IDE or Editor features supports

#### Useful links
- https://git.javra.com/swaiba/pf-pwa.git
- https://github.com/magento-research/pwa-studio
- https://magento-research.github.io/pwa-studio/venia-pwa-concept/setup/